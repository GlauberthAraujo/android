package voluntarios.es.senac.br.livrariatgm;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CarrinhoAdapter extends BaseAdapter {
    private final List<Livro> livros;
    private Activity act;

    public CarrinhoAdapter(List<Livro> livros, Activity act) {
        this.livros = livros;
        this.act = act;
    }

    @Override
    public int getCount() {
        return livros.size();
    }

    @Override
    public Object getItem(int position) {
        return livros.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = act.getLayoutInflater().inflate(R.layout.activity_carrinho, parent, false);
        Livro livro = livros.get(position);

        //pegando as referências das Views
        TextView titulo = (TextView)
                view.findViewById(R.id.titulo);
        TextView preco = (TextView)
                view.findViewById(R.id.preco);
        ImageView imagem = (ImageView)
                view.findViewById(R.id.imagem);

        //populando as Views
        titulo.setText(livro.getTitulo());
        preco.setText("R$ " +livro.getPreco());

        int id = livro.getId();

        switch (id){
            case 1:       imagem.setImageResource(R.drawable.ic_assassins);
            break;

            case 2:       imagem.setImageResource(R.drawable.ic_percy);
            break;

            case 3:       imagem.setImageResource(R.drawable.ic_boxharry);
            break;

        }



        return view;
    }
}
