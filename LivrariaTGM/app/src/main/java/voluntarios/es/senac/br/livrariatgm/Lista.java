package voluntarios.es.senac.br.livrariatgm;

import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

public class Lista extends AppCompatActivity {

    public  static List<Livro> lista = new ArrayList<>( );

   public Livro a = new Livro(1, "Assassin's Creed II", 32.50, R.drawable.ic_assassins);
   public Livro p = new Livro(2, "Percy Jackson e os Olimpianos", 28.40, R.drawable.ic_percy);
   public Livro bh = new Livro(3, "Box Harry Potter", 59.25, R.drawable.ic_boxharry);

    public Lista() {
        lista.add(bh);
        lista.add(p);
       lista.add(a);
    }
}
