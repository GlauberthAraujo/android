package voluntarios.es.senac.br.livrariatgm;

public class Livro {

    private int id;
    private String titulo;
    private String autor;
    private double preco;
    private int imagem;

    public Livro() {

    }

    public Livro(int id , String titulo, double preco, int imagem) {
        this.titulo = titulo;
        this.id = id;
        this.preco = preco;
        this.imagem = imagem;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return imagem + titulo + "/n" + preco;
    }

    public int getImagem() {
        return imagem;
    }

    public void setImagem(int imagem) {
        this.imagem = imagem;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }
}
