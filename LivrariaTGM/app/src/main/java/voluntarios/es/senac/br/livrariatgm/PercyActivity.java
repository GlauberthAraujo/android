package voluntarios.es.senac.br.livrariatgm;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class PercyActivity extends AppCompatActivity {

    TextView tv_preco;
    TextView tv_titulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_percy);
        tv_preco = findViewById(R.id.tv_preco);
        tv_titulo = findViewById(R.id.tv_titulo);
    }

    public void comprar(View view){
        alertar();
    }

    public void alertar (){

        AlertDialog.Builder alert = new AlertDialog.Builder(PercyActivity.this).setTitle("Você está comprando:");
        LayoutInflater factory = LayoutInflater.from(PercyActivity.this);
        final View view = factory.inflate(R.layout.dialog, null);
        alert.setView(view);

        TextView titulo = (TextView) view.findViewById(R.id.d_titulo);
        titulo.setText(tv_titulo.getText());

        TextView preco = (TextView) view.findViewById(R.id.d_preco);
        preco.setText( tv_preco.getText());


        alert.setCancelable(false)
                .setIcon(R.drawable.ic_percy)
                .setPositiveButton("Comprar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(),"Obrigado por comprar conosco!", Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        alert.show();

    }

    public void adicionarCarrinho(View view){

        Toast.makeText(this, tv_titulo.getText() +" adicionado ao carrinho!", Toast.LENGTH_LONG).show();

    }
}
