package voluntarios.es.senac.br.livrariatgm;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class RomanceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_romance);
    }

    public void verLivroA (View view){
        Intent intent = new Intent(this,AssassinsActivity.class);
        startActivity(intent);
    }
    public void verLivroP (View view){
        Intent intent = new Intent(this,PercyActivity.class);
        startActivity(intent);
    }
    public void verLivroBP (View view){
        Intent intent = new Intent(this,AssassinsActivity.class);
        startActivity(intent);
    }
    public void verLivroBH (View view){
        Intent intent = new Intent(this,AssassinsActivity.class);
        startActivity(intent);
    }

    public void adicionarCarrinho(View view){

        Toast.makeText(this,"Adicionado ao carrinho!", Toast.LENGTH_LONG).show();

    }

}
