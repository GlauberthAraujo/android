package voluntarios.es.senac.br.livrariatgm;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class CarrinhoActivity extends Lista {

    double total ;
   /* Livro a = new Livro(1, "Assassin's Creed II", 32.50, R.drawable.ic_assassins);
    Livro p = new Livro(2, "Percy Jackson e os Olimpianos", 28.40, R.drawable.ic_percy);
    Livro bh = new Livro(3, "Box Harry Potter", 59.25, R.drawable.ic_boxharry);*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);

        final ListView listview = (ListView) findViewById(R.id.lista);
        // ArrayAdapter<Livro> adapter = new ArrayAdapter<Livro>(this, android.R.layout.simple_list_item_1, lista);
        CarrinhoAdapter adapter = new CarrinhoAdapter(Lista.lista, this);
        listview.setAdapter(adapter);

        Button bt = findViewById(R.id.bt_preco);

        total = a.getPreco() + p.getPreco() + bh.getPreco();

        bt.setText("R$ " + total);

    }

    public void comprar(View view){
        alertar();
    }

    public void alertar (){

        AlertDialog.Builder alert = new AlertDialog.Builder(this).setTitle("Você está comprando:");
        LayoutInflater factory = LayoutInflater.from(this);
        final View view = factory.inflate(R.layout.dialog, null);
        alert.setView(view);

        TextView titulo = (TextView) view.findViewById(R.id.d_titulo);
        titulo.setText(lista.size() + " itens");

        TextView preco = (TextView) view.findViewById(R.id.d_preco);
        preco.setText( "R$ " + total);


        alert.setCancelable(false)
                .setIcon(R.drawable.ic_car)
                .setPositiveButton("Comprar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getApplicationContext(),"Obrigado por comprar conosco!", Toast.LENGTH_LONG).show();

                }})
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        alert.show();

    }

    public void excluir(View v){
      remover();
    }

    public void remover(){
        AlertDialog.Builder alert = new AlertDialog.Builder(this).setTitle("Deseja mesmo remover este item?");
        LayoutInflater factory = LayoutInflater.from(this);
        final View view = factory.inflate(R.layout.dialog, null);
        alert.setView(view);


        alert.setCancelable(true)
                .setIcon(R.drawable.ic_excluir)
                .setPositiveButton("Remover", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final ListView listview = (ListView) findViewById(R.id.lista);
                        CarrinhoAdapter adapter = new CarrinhoAdapter(Lista.lista, CarrinhoActivity.this);
                        Lista.lista.remove(a);
                        listview.setAdapter(adapter);

                        Toast.makeText(getApplicationContext(),"Obrigado por comprar conosco!", Toast.LENGTH_LONG).show();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        alert.show();
    }

}
